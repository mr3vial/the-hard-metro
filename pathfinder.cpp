#pragma once

#include "pathfinder.h"

size_t find_optimal_time(Graph& graph,
                         const std::string& from_station,
                         const std::string& from_line,
                         const std::string& to_station,
                         const std::string& to_line,
                         size_t minute) {
  if (!graph.check_station(from_station, from_line) ||
      !graph.check_station(to_station, to_line)) {
    return minute;
  }

  std::priority_queue<std::tuple<size_t, size_t, size_t>,
                      std::vector<std::tuple<size_t, size_t, size_t>>,
                      std::greater<std::tuple<size_t, size_t, size_t>>> weights_heap;
  std::vector<size_t> distances(graph.get_vertices_number(), kLimit);
  std::vector<size_t> transfers(graph.get_vertices_number(), kLimit);

  size_t from_code = graph.get_code(from_station, from_line);
  size_t to_code = graph.get_code(to_station, to_line);
  weights_heap.emplace(minute, from_code, 0);
  distances[from_code] = minute;

  while (!weights_heap.empty()) {
    auto [current_time, current_station, last_time] = weights_heap.top();
    weights_heap.pop();
    if (current_time <= distances[current_station]) {
      auto neighbours = graph.get_neighbours(current_station);
      for (auto it = neighbours.begin(); it != neighbours.end(); ++it) {
        const auto& [next_station, current_weight] = (*it);
        size_t time_to_enter = 0;
        if (last_time == 0 && current_weight > 0) {
          auto delays = graph.get_delays(current_station);
          size_t delay_to_choose;

          if (current_station < next_station) {
            delay_to_choose = delays.first;
          } else {
            delay_to_choose = delays.second;
          }

          if (current_time < delay_to_choose) {
            time_to_enter = delay_to_choose - current_time;
          } else {
            time_to_enter = (current_time - delay_to_choose) % graph.get_time_to_wait(current_station);
          }

        }

        if (distances[current_station] + current_weight + time_to_enter < distances[next_station]) {
          transfers[next_station] = current_station;
          distances[next_station] = distances[current_station] + current_weight + time_to_enter;
          weights_heap.emplace(distances[next_station], next_station, current_weight);
        }
      }
    }
  }

  std::vector<std::pair<std::string, std::string>> route;

  size_t station = to_code;
  while (station != from_code) {
    route.emplace_back(graph.get_station_name(station), graph.get_station_line(station));
    station = transfers[station];
  }

  std::cout << "Optimal stations to reach:" << std::endl;

  for (int i = static_cast<int>(route.size()) - 1; i >= 0; --i) {
    std::cout << route[i].first << ", " << route[i].second << std::endl;
  }

  return distances[to_code];
}
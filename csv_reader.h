#pragma once

#include <fstream>
#include <sstream>
#include <tuple>
#include <vector>

static const char kDelimiter = ';';

std::vector<std::vector<std::string>> read_csv (const std::string& filename,
                                                char delimiter = kDelimiter);
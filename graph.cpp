#pragma once

#include "graph.h"

enum map_columns {
  kStationName = 0,
  kStationLine = 1,
  kIsLineCircle = 2,
  kNext = 3,
  kIsLast = 4,
  kToWait = 5,
};

enum transfer_columns {
  kFirstStationName = 0,
  kFirstStationLine = 1,
  kSecondStationName = 2,
  kSecondStationLine = 3,
};

void Graph::init_map(const std::vector<std::vector<std::string>>& map_info,
                     const std::vector<std::vector<std::string>>& transfers_info) {
  size_t line_start = 0;
  size_t total_lines = 1;
  size_t delay_from = 0;

  for (size_t i = 1; i < map_info.size(); ++i) {
    if (!codes_[std::make_pair(map_info[i][kStationName],
                               map_info[i][kStationLine])]) {
      codes_[std::make_pair(map_info[i][kStationName],
                            map_info[i][kStationLine])] = i - 1;
      stations_[i - 1] = map_info[i][kStationName];
      lines_[i - 1] = map_info[i][kStationLine];
    }

    vertices_info_[i - 1].group = total_lines;
    vertices_info_[i - 1].to_wait = std::stoul(map_info[i][kToWait]);
    vertices_info_[i - 1].delay_from = delay_from;

    if (map_info[i][kIsLast] != "1") {
      set_edge(i - 1, i, std::stoul(map_info[i][kNext]));
      delay_from += std::stoul(map_info[i][kNext]);
    } else if (map_info[i][2] == "1") {
      set_edge(i - 1, line_start, std::stoul(map_info[i][kNext]));
      vertices_info_[line_start].delay_to = 0;
      size_t delay_to = std::stoul(map_info[i][kNext]);
      for (size_t j = i - 1; j > line_start; --j) {
        vertices_info_[j].delay_to = delay_to;
        delay_to += std::stoul(map_info[j][kNext]);
      }
      delay_from = 0;
      line_start = i;
      ++total_lines;
    } else {
      size_t delay_to = 0;
      for (size_t j = i - 1; j > line_start; --j) {
        vertices_info_[j].delay_to = delay_to;
        delay_to += std::stoul(map_info[j][kNext]);
      }
      vertices_info_[line_start].delay_to = delay_to;
      delay_from = 0;
      line_start = i;
      ++total_lines;
    }
  }

  for (size_t i = 1; i < transfers_info.size(); ++i) {
    set_edge(codes_[std::make_pair(transfers_info[i][kFirstStationName],
                                   transfers_info[i][kFirstStationLine])],
             codes_[std::make_pair(transfers_info[i][kSecondStationName],
                                   transfers_info[i][kSecondStationLine])],
             0);
    vertices_info_[codes_[std::make_pair(transfers_info[i][kFirstStationName],
                                       transfers_info[i][kFirstStationLine])]].is_hub = true;
    vertices_info_[codes_[std::make_pair(transfers_info[i][kSecondStationName],
                                         transfers_info[i][kSecondStationLine])]].is_hub = true;
  }
}

size_t Graph::get_code(const std::string& name, const std::string& line) {
  std::pair<std::string, std::string> station(name, line);
  if (codes_.count(station) > 0) {
    return codes_[station];
  }
  return -1;
}

void Graph::set_edge(size_t first, size_t second, size_t weight) {
  adjacent_[first].emplace_back(second, weight);
  adjacent_[second].emplace_back(first, weight);
}

size_t Graph::get_vertices_number() const {
  return adjacent_.size();
}

const std::vector<std::pair<size_t, size_t>>& Graph::get_neighbours(size_t code) const {
  return adjacent_[code];
}

size_t Graph::get_time_to_wait(size_t code) const {
  return vertices_info_[code].to_wait;
}

std::pair<size_t, size_t> Graph::get_delays(size_t code) const {
  return std::make_pair(vertices_info_[code].delay_from,
                        vertices_info_[code].delay_to);
}

std::string Graph::get_station_name(size_t code) const {
  return stations_[code];
}

std::string Graph::get_station_line(size_t code) const {
  return lines_[code];
}

bool Graph::check_station(const std::string& name, const std::string& line) {
  return codes_.count(std::make_pair(name, line)) > 0;
}
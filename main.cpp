#include "csv_reader.h"
#include "graph.h"
#include "pathfinder.h"

#include <iostream>

int main() {
  auto map_info = read_csv("metro_map.csv");
  auto transfers_info = read_csv("metro_transfers.csv");
  Graph metro_graph(map_info.size() - 1);
  metro_graph.init_map(map_info, transfers_info);

  std::string from_station, from_line, to_station, to_line, after_msg;
  size_t minute;
  std::cout << "Please enter the dispatching station" << std::endl;
  std::getline(std::cin, from_station);
  std::cout << "Please enter the line of the dispatching station" << std::endl;
  std::getline(std::cin, from_line);
  std::cout << "Please enter the destination station" << std::endl;
  std::getline(std::cin, to_station);
  std::cout << "Please enter the line of the destination station" << std::endl;
  std::getline(std::cin, to_line);
  std::cout << "Please enter the minute" << std::endl;
  std::cin >> minute;
  std::cout << "The optimal route is:" << std::endl;
  std::cout << find_optimal_time(metro_graph,
                                 from_station,
                                 from_line,
                                 to_station,
                                 to_line,
                                 minute) - minute << " minutes" << std::endl;

  return 0;
}

#pragma once

#include "graph.h"

#include <algorithm>
#include <iterator>
#include <iostream>
#include <limits>
#include <queue>
#include <string>
#include <tuple>
#include <vector>

static const size_t kLimit = std::numeric_limits<size_t>::max() / 2;

size_t find_optimal_time(Graph& graph,
                         const std::string& from_station,
                         const std::string& from_line,
                         const std::string& to_station,
                         const std::string& to_line,
                         size_t minute);
#pragma once

#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

struct VerticesInfo {
  size_t group;
  size_t to_wait;
  size_t delay_from;
  size_t delay_to;
  bool is_hub;
};

struct StringPairHash {
  uint64_t operator()(const std::pair<std::string, std::string>& string_pair) const {
    auto first = std::hash<std::string>{}(string_pair.first);
    auto second = std::hash<std::string>{}(string_pair.second);
    return first ^ second;
  }
};

class Graph {
 public:
  Graph(size_t vertices_number) : adjacent_(vertices_number,
                                            std::vector<std::pair<size_t, size_t>>(0)),
                                  vertices_info_(vertices_number),
                                  stations_(vertices_number),
                                  lines_(vertices_number) {}
  ~Graph() = default;
  Graph& operator=(const Graph&) = default;

  void init_map(const std::vector<std::vector<std::string>>& map_info,
                const std::vector<std::vector<std::string>>& transfers_info);

  size_t get_code(const std::string& name, const std::string& line);
  void set_edge(size_t first, size_t second, size_t weight);

  size_t get_vertices_number() const;
  size_t get_time_to_wait(size_t code) const;
  std::pair<size_t, size_t> get_delays(size_t code) const;

  const std::vector<std::pair<size_t, size_t>>& get_neighbours(size_t code) const;
  std::string get_station_name(size_t code) const;
  std::string get_station_line(size_t line) const;

  bool check_station(const std::string& name, const std::string& line);

 private:
  std::vector<std::vector<std::pair<size_t, size_t>>> adjacent_;
  std::unordered_map<std::pair<std::string, std::string>, size_t, StringPairHash> codes_;
  std::vector<std::string> stations_;
  std::vector<std::string> lines_;
  std::vector<VerticesInfo> vertices_info_;
};
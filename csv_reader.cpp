#pragma once

#include "csv_reader.h"

std::vector<std::vector<std::string>> read_csv (const std::string& filename,
                                                char delimiter) {
  std::vector<std::vector<std::string>> result;
  std::vector<std::string> current_row;
  std::string current_line;
  std::string current_word;

  std::fstream current_file;
  current_file.open(filename, std::fstream::in);
  while (std::getline(current_file, current_line)) {
    current_row.clear();
    std::stringstream str(current_line);
    while (std::getline(str, current_word, delimiter)) {
      current_row.push_back(current_word);
    }
    result.push_back(current_row);
  }

  return result;
}